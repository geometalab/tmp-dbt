Welcome to your new dbt project!

### Using the starter project

Try running the following commands:

- dbt run
- dbt test


### Resources:

- Learn more about dbt [in the docs](https://docs.getdbt.com/docs/introduction)
- Check out [Discourse](https://discourse.getdbt.com/) for commonly asked questions and answers
- Join the [chat](http://slack.getdbt.com/) on Slack for live discussions and support
- Find [dbt events](https://events.getdbt.com) near you
- Check out [the blog](https://blog.getdbt.com/) for the latest news on dbt's development and best practices

### Run example (using dbt)

```bash
docker run --rm -it --network=host -v $(pwd)/profiles.yml:/root/.dbt/profiles.yml -v $(pwd):/usr/app --entrypoint='' fishtownanalytics/dbt:0.19.1 bash
# inside the container:
dbt run --full-refresh
```
